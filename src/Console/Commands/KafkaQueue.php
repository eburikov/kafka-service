<?php

namespace Xplatform\KafkaService\Console\Commands;

use Illuminate\Console\Command;
use Xplatform\KafkaService\KafkaService;

class KafkaQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kafka:queue {group_name?} {--offset_reset=earliest}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \App::make(KafkaService::class)->consume(
            $this->argument('group_name'),
            $this->option('offset_reset')
        );
    }
}
