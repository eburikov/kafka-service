<?php
namespace Xplatform\KafkaService\Contracts;

use Junges\Kafka\Contracts\KafkaConsumerMessage;

interface KafkaTopicHandler {
    public function handle (KafkaConsumerMessage $message);
}
