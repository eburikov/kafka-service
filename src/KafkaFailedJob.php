<?php
namespace Xplatform\KafkaService;
use Illuminate\Database\ConnectionResolverInterface;

class KafkaFailedJob {
    private $connection;
    public function __construct($connection = \DB::class) {
        $this->connection = $connection;
    }

    public function logPublishFailure(String $topic, Array $payload, \Exception $exception) {
        $this->connection::table('failed_kafka_jobs')->insert([
            'action' => 'publish',
            'topic' => $topic,
            'payload' => json_encode($payload),
            'exception' => $exception->getMessage()
        ]);
    }

    public function logConsumeFailure(String $topic, Array $payload, \Exception $exception) {
        $this->connection::table('failed_kafka_jobs')->insert([
            'action' => 'consume',
            'topic' => $topic,
            'payload' => json_encode($payload),
            'exception' => $exception->getMessage()
        ]);
    }
}
