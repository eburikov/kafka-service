<?php
namespace Xplatform\KafkaService;

use Junges\Kafka\Facades\Kafka;
use Junges\Kafka\Message\Message;
use Junges\Kafka\Message\Registry\AvroSchemaRegistry;
use Junges\Kafka\Message\Serializers\AvroSerializer;
use Junges\Kafka\Message\Deserializers\AvroDeserializer;
use Junges\Kafka\Message\KafkaAvroSchema;
use Junges\Kafka\Contracts\KafkaAvroSchemaRegistry;
use Junges\Kafka\Contracts\KafkaConsumerMessage;
use Junges\Kafka\Handlers\RetryableHandler;
use Junges\Kafka\Handlers\RetryStrategies\DefaultRetryStrategy;
use Junges\Kafka\Commit\NativeSleeper;

use FlixTech\AvroSerializer\Objects\RecordSerializer;
use FlixTech\SchemaRegistryApi\Registry\CachedRegistry;
use FlixTech\SchemaRegistryApi\Registry\BlockingRegistry;
use FlixTech\SchemaRegistryApi\Registry\PromisingRegistry;
use FlixTech\SchemaRegistryApi\Registry\Cache\AvroObjectCacheAdapter;
use GuzzleHttp\Client;

use Xplatform\KafkaService\Contracts\KafkaTopicHandler;

class KafkaService
{
    private $configOptions = [];
    private $registry;
    private $cachedRegistry;
    private $recordSerializer;
    private $handlers = [];

    public function __construct () {
        if (!$this->isConfigured()) return;
        $this->setConfigOptions();
        $this->setCachedRegistry();
        $this->setRegistry();
        $this->setRecordSerializer();
    }

    public function publish (
        String $topic,
        Array $payload,
        Int $version = KafkaAvroSchemaRegistry::LATEST_VERSION
    ): void {
        $producer = Kafka::publishOn($topic)
            ->usingSerializer($this->getSerializerForTopic($topic, $version))
            ->withConfigOptions($this->configOptions);

        $producer = $this->setBodyForProducer($producer, $payload);

        try { $producer->send(); }
        catch (\Exception $e) {
            (new KafkaFailedJob())->logPublishFailure($topic, $payload, $e);
        }
    }

    public function consume (String $group = null, String $offsetReset = 'earliest') {
        // if (!$group) $group=
        $consumer = Kafka::createConsumer($this->getTopicsToConsume(), $group)
        ->withOptions($this->configOptions)
        ->withOptions([
            'auto.offset.reset' => $offsetReset
        ])
        ->usingDeserializer($this->getDeserializer())
        ->withHandler(new RetryableHandler(function(KafkaConsumerMessage $message) {
            $this->getHandlerForTopic($message->getTopicName())->handle($message);
        }, new DefaultRetryStrategy(), new NativeSleeper()))
        // ->withHandler(function(KafkaConsumerMessage $message) {
        //     try {
        //         $this->getHandlerForTopic($message->getTopicName())->handle($message);
        //     } catch (\Exception $e) {
        //         (new KafkaFailedJob())->logConsumeFailure(
        //             $message->getTopicName(), $message->getBody(), $e
        //         );
        //     }
        // })
        // ->withDlq()
        ->build();

        $consumer->consume();
    }

    public function registerHandlers(Array $handlers) {
        foreach ($handlers as $topic => $handler) {
            list($handlerClass, $version) = $this->parseHandler($handler);
            $this->registerHandler($topic, $handlerClass, $version);
        }
    }

    private function parseHandler ($handler): Array {
        if (is_array($handler)) {
            return [
                new $handler[0],
                $handler[1],
            ];
        }

        return [
            new $handler,
            KafkaAvroSchemaRegistry::LATEST_VERSION
        ];
    }

    private function getHandlerForTopic (String $topic) {
        return $this->handlers[$topic];
    }

    public function getTopicsToConsume () {
        return array_keys($this->handlers);
    }

    private function registerHandler (String $topic, KafkaTopicHandler $handler, Int $version = KafkaAvroSchemaRegistry::LATEST_VERSION) {
        $this->handlers[$topic] = $handler;

        $this->getDeserializerForTopic($topic, $version);
    }

    private function getDeserializer () {
        try {
            return new AvroDeserializer($this->registry, $this->recordSerializer /*, AvroEncoderInterface::ENCODE_BODY */);
        } catch (\Exception $e) {
            dd ('caught in deserializ');
        }
    }

    private function getSerializerForTopic($topic, $version = KafkaAvroSchemaRegistry::LATEST_VERSION) {
        $this->registry->addBodySchemaMappingForTopic(
            $topic,
            new KafkaAvroSchema($topic.'-value', $version /*,AvroSchema $definition */)
        );

        $this->registry->addKeySchemaMappingForTopic(
            $topic,
            new KafkaAvroSchema($topic.'-key', $version, /* AvroSchema $definition */)
        );

        return new AvroSerializer($this->registry, $this->recordSerializer /*, AvroEncoderInterface::ENCODE_BODY */);
    }

    private function getDeserializerForTopic($topic, $version = KafkaAvroSchemaRegistry::LATEST_VERSION) {
        $this->registry->addBodySchemaMappingForTopic(
            $topic,
            new KafkaAvroSchema($topic.'-value', $version /*,AvroSchema $definition */)
        );

        $this->registry->addKeySchemaMappingForTopic(
            $topic,
            new KafkaAvroSchema($topic.'-key', $version, /* AvroSchema $definition */)
        );

        return new AvroDeserializer($this->registry, $this->recordSerializer /*, AvroEncoderInterface::ENCODE_BODY */);
    }

    private function setCachedRegistry (): void {
        if (!$this->isConfigured()) return;
        $this->cachedRegistry = new CachedRegistry(
            new BlockingRegistry(
                new PromisingRegistry(
                    new Client([
                        'base_uri' => config('kafka')['registry_options']['registry.url'],
                        'auth' => [
                            config('kafka')['registry_options']['registry.username'],
                            config('kafka')['registry_options']['registry.password']
                        ],
                        'verify' => config('kafka')['registry_options']['ssl.ca.location'] || null
                    ])
                )
            ),
            new AvroObjectCacheAdapter()
        );
    }

    private function setRegistry (): void {
        $this->registry = new AvroSchemaRegistry($this->cachedRegistry);
    }

    private function setRecordSerializer (): void {
        $this->recordSerializer = new RecordSerializer($this->cachedRegistry);
    }

    private function setConfigOptions (): void {
        foreach (config('kafka')['options'] as $option => $value ) {
            $this->setConfigOption($option, $value);
        }
    }

    private function setConfigOption ($option, $value): void {
        if ($value) {
            $this->configOptions[$option] = $value;
        }
    }

    private function setBodyForProducer($producer, Array $payload) {
        foreach ($payload as $key => $value) {
            $producer = $producer->withBodyKey($key, $value);
        }
        return $producer;
    }

    private function isConfigured () {
        return !!config('kafka');
    }
}
