<?php

namespace Xplatform\KafkaService\Providers;

use Illuminate\Support\ServiceProvider;
use Xplatform\KafkaService\KafkaService;
use Xplatform\KafkaService\Console\Commands\KafkaQueue;

class KafkaServiceProvider extends ServiceProvider
{
    protected $handlers = [];
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->app->singleton(KafkaService::class, function ($app) {
            return new KafkaService();
        });

        $this->handlers = config('kafka') ? config('kafka')['handlers'] : [];
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__."/../../config/kafka.php" => config_path('kafka.php'),
        ], 'kafka-service-config');

        $this->app->make(KafkaService::class)->registerHandlers($this->handlers);

        if ($this->app->runningInConsole()) {
            $this->commands([
                KafkaQueue::class,
            ]);
        }
    }
}
