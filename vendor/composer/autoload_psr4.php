<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'voku\\' => array($vendorDir . '/voku/portable-ascii/src/voku'),
    'Xplatform\\KafkaService\\' => array($baseDir . '/src'),
    'Widmogrod\\' => array($vendorDir . '/widmogrod/php-functional/src'),
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Contracts\\Translation\\' => array($vendorDir . '/symfony/translation-contracts'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/src'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-factory/src', $vendorDir . '/psr/http-message/src'),
    'Psr\\Http\\Client\\' => array($vendorDir . '/psr/http-client/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'Junges\\Kafka\\' => array($vendorDir . '/mateusjunges/laravel-kafka/src'),
    'Illuminate\\Support\\' => array($vendorDir . '/illuminate/collections', $vendorDir . '/illuminate/conditionable', $vendorDir . '/illuminate/macroable', $vendorDir . '/illuminate/support'),
    'Illuminate\\Contracts\\' => array($vendorDir . '/illuminate/contracts'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'FunctionalPHP\\' => array($vendorDir . '/functional-php/fantasy-land/src'),
    'FlixTech\\SchemaRegistryApi\\' => array($vendorDir . '/flix-tech/confluent-schema-registry-api/src'),
    'FlixTech\\AvroSerializer\\Objects\\' => array($vendorDir . '/flix-tech/avro-serde-php/src/Objects'),
    'FlixTech\\AvroSerializer\\Integrations\\' => array($vendorDir . '/flix-tech/avro-serde-php/integrations'),
    'Doctrine\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib/Doctrine/Inflector'),
    'Carbon\\' => array($vendorDir . '/nesbot/carbon/src/Carbon'),
    'Assert\\' => array($vendorDir . '/beberlei/assert/lib/Assert'),
);
